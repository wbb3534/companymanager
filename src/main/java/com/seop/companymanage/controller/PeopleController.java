package com.seop.companymanage.controller;

import com.seop.companymanage.model.PeopleItem;
import com.seop.companymanage.model.PeopleRequest;
import com.seop.companymanage.service.PeopleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/people")
@RequiredArgsConstructor
public class PeopleController {
    private final PeopleService peopleService;

    @PostMapping("/people")
    public String setPeople(@RequestBody @Valid PeopleRequest request) {
        peopleService.setPeople(request.getName(), request.getPhone(), request.getBirthday());
        return "등록됨";
    }

    @GetMapping("/people-info")
    public List<PeopleItem> getPeopleInfo() {
        List<PeopleItem> result = peopleService.getPeopleInfo();
        return result;
    }
}
