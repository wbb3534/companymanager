package com.seop.companymanage.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Setter
@Getter
public class PeopleRequest {
    @NotNull
    @Length(min = 1, max = 20)
    private String name;
    @NotNull
    @Length(min = 11, max = 13)
    private String phone;
    @NotNull
    @Valid
    private LocalDate birthday;
}
