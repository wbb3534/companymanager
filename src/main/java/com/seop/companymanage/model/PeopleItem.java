package com.seop.companymanage.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PeopleItem {
    private String name;
    private String phone;
}
