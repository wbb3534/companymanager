package com.seop.companymanage.repository;

import com.seop.companymanage.entity.People;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeopleRepository extends JpaRepository<People, Long> {
}
