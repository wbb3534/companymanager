package com.seop.companymanage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompanyManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(CompanyManageApplication.class, args);
    }

}
